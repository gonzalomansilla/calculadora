package calculadora_tp1_2019;

import calculadora_tp1_2019.clases.Calculadora;
import calculadora_tp1_2019.clases.Cuenta;
import calculadora_tp1_2019.funcionalidadesExtras.SmartScroller;
import calculadora_tp1_2019.interfaces.Observable;
import calculadora_tp1_2019.interfaces.Observer;
import javax.swing.JOptionPane;

public class JF_Calculadora extends javax.swing.JFrame implements Observer {

  private Calculadora calculadora;
  private Observable observable;

  public JF_Calculadora() {
    initComponents();

    calculadora = new Calculadora();

    observable = calculadora;
    observable.establecerObserver(this);

    text_resul.setText(calculadora.consultarResultado());
    
    new SmartScroller(scrollPanel, SmartScroller.HORIZONTAL, SmartScroller.END);
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    panel_botones = new javax.swing.JPanel();
    panel_numeros = new javax.swing.JPanel();
    btn_7 = new javax.swing.JButton();
    btn_8 = new javax.swing.JButton();
    btn_9 = new javax.swing.JButton();
    btn_4 = new javax.swing.JButton();
    btn_5 = new javax.swing.JButton();
    btn_6 = new javax.swing.JButton();
    btn_1 = new javax.swing.JButton();
    btn_2 = new javax.swing.JButton();
    btn_3 = new javax.swing.JButton();
    btn_puntoDecimal = new javax.swing.JButton();
    btn_0 = new javax.swing.JButton();
    btn_igualdad = new javax.swing.JButton();
    panel_memoria = new javax.swing.JPanel();
    btn_memory = new javax.swing.JButton();
    btn_save = new javax.swing.JButton();
    panel_operaciones = new javax.swing.JPanel();
    btn_producto = new javax.swing.JButton();
    btn_division = new javax.swing.JButton();
    btn_suma = new javax.swing.JButton();
    btn_resta = new javax.swing.JButton();
    panel_borrado = new javax.swing.JPanel();
    btn_del = new javax.swing.JButton();
    btn_ac = new javax.swing.JButton();
    panel_display = new javax.swing.JPanel();
    text_resul = new javax.swing.JTextField();
    scrollPanel = new javax.swing.JScrollPane();
    text_calculo = new javax.swing.JTextField();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setTitle("Calculadora");
    setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    setMinimumSize(new java.awt.Dimension(150, 200));
    setName("main"); // NOI18N
    setResizable(false);
    setSize(new java.awt.Dimension(450, 525));
    getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    panel_botones.setBackground(new java.awt.Color(42, 42, 42));
    panel_botones.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    panel_numeros.setBackground(new java.awt.Color(42, 42, 42));
    panel_numeros.setPreferredSize(new java.awt.Dimension(350, 400));
    panel_numeros.setLayout(new java.awt.GridLayout(4, 3, 5, 5));

    btn_7.setBackground(new java.awt.Color(255, 255, 255));
    btn_7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_7.setForeground(new java.awt.Color(42, 42, 42));
    btn_7.setText("7");
    btn_7.setFocusable(false);
    btn_7.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_7.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_7ActionPerformed(evt);
      }
    });
    panel_numeros.add(btn_7);

    btn_8.setBackground(new java.awt.Color(255, 255, 255));
    btn_8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_8.setForeground(new java.awt.Color(42, 42, 42));
    btn_8.setText("8");
    btn_8.setFocusable(false);
    btn_8.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_8.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_8ActionPerformed(evt);
      }
    });
    panel_numeros.add(btn_8);

    btn_9.setBackground(new java.awt.Color(255, 255, 255));
    btn_9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_9.setForeground(new java.awt.Color(42, 42, 42));
    btn_9.setText("9");
    btn_9.setFocusable(false);
    btn_9.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_9.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_9ActionPerformed(evt);
      }
    });
    panel_numeros.add(btn_9);

    btn_4.setBackground(new java.awt.Color(255, 255, 255));
    btn_4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_4.setForeground(new java.awt.Color(42, 42, 42));
    btn_4.setText("4");
    btn_4.setFocusable(false);
    btn_4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btn_4.setOpaque(false);
    btn_4.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_4.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_4ActionPerformed(evt);
      }
    });
    panel_numeros.add(btn_4);

    btn_5.setBackground(new java.awt.Color(255, 255, 255));
    btn_5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_5.setForeground(new java.awt.Color(42, 42, 42));
    btn_5.setText("5");
    btn_5.setFocusable(false);
    btn_5.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_5.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_5ActionPerformed(evt);
      }
    });
    panel_numeros.add(btn_5);

    btn_6.setBackground(new java.awt.Color(255, 255, 255));
    btn_6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_6.setForeground(new java.awt.Color(42, 42, 42));
    btn_6.setText("6");
    btn_6.setFocusable(false);
    btn_6.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_6.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_6ActionPerformed(evt);
      }
    });
    panel_numeros.add(btn_6);

    btn_1.setBackground(new java.awt.Color(255, 255, 255));
    btn_1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_1.setForeground(new java.awt.Color(42, 42, 42));
    btn_1.setText("1");
    btn_1.setFocusable(false);
    btn_1.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_1ActionPerformed(evt);
      }
    });
    panel_numeros.add(btn_1);

    btn_2.setBackground(new java.awt.Color(255, 255, 255));
    btn_2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_2.setForeground(new java.awt.Color(42, 42, 42));
    btn_2.setText("2");
    btn_2.setFocusable(false);
    btn_2.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_2.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_2ActionPerformed(evt);
      }
    });
    panel_numeros.add(btn_2);

    btn_3.setBackground(new java.awt.Color(255, 255, 255));
    btn_3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_3.setForeground(new java.awt.Color(42, 42, 42));
    btn_3.setText("3");
    btn_3.setFocusable(false);
    btn_3.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_3.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_3ActionPerformed(evt);
      }
    });
    panel_numeros.add(btn_3);

    btn_puntoDecimal.setBackground(new java.awt.Color(255, 255, 255));
    btn_puntoDecimal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_puntoDecimal.setForeground(new java.awt.Color(42, 42, 42));
    btn_puntoDecimal.setText(".");
    btn_puntoDecimal.setFocusable(false);
    btn_puntoDecimal.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_puntoDecimal.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_puntoDecimalActionPerformed(evt);
      }
    });
    panel_numeros.add(btn_puntoDecimal);

    btn_0.setBackground(new java.awt.Color(255, 255, 255));
    btn_0.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_0.setForeground(new java.awt.Color(42, 42, 42));
    btn_0.setText("0");
    btn_0.setFocusable(false);
    btn_0.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_0.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_0ActionPerformed(evt);
      }
    });
    panel_numeros.add(btn_0);

    btn_igualdad.setBackground(new java.awt.Color(22, 128, 181));
    btn_igualdad.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_igualdad.setForeground(new java.awt.Color(42, 42, 42));
    btn_igualdad.setText("=");
    btn_igualdad.setFocusable(false);
    btn_igualdad.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_igualdad.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_igualdadActionPerformed(evt);
      }
    });
    panel_numeros.add(btn_igualdad);

    panel_botones.add(panel_numeros, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 320, 310));

    panel_memoria.setBackground(new java.awt.Color(42, 42, 42));
    panel_memoria.setLayout(new java.awt.GridLayout(1, 2, 5, 5));

    btn_memory.setBackground(new java.awt.Color(51, 51, 51));
    btn_memory.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_memory.setForeground(new java.awt.Color(255, 255, 255));
    btn_memory.setText("Memory");
    btn_memory.setEnabled(false);
    btn_memory.setFocusable(false);
    btn_memory.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_memory.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_memoryActionPerformed(evt);
      }
    });
    panel_memoria.add(btn_memory);

    btn_save.setBackground(new java.awt.Color(51, 51, 51));
    btn_save.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_save.setForeground(new java.awt.Color(255, 255, 255));
    btn_save.setText("Save");
    btn_save.setEnabled(false);
    btn_save.setFocusable(false);
    btn_save.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_save.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_saveActionPerformed(evt);
      }
    });
    panel_memoria.add(btn_save);

    panel_botones.add(panel_memoria, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 210, 70));

    panel_operaciones.setBackground(new java.awt.Color(42, 42, 42));
    panel_operaciones.setLayout(new java.awt.GridLayout(4, 1, 5, 5));

    btn_producto.setBackground(new java.awt.Color(204, 204, 204));
    btn_producto.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_producto.setForeground(new java.awt.Color(42, 42, 42));
    btn_producto.setText("x");
    btn_producto.setFocusable(false);
    btn_producto.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_producto.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_productoActionPerformed(evt);
      }
    });
    panel_operaciones.add(btn_producto);

    btn_division.setBackground(new java.awt.Color(204, 204, 204));
    btn_division.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_division.setForeground(new java.awt.Color(42, 42, 42));
    btn_division.setText("/");
    btn_division.setFocusable(false);
    btn_division.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_division.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_divisionActionPerformed(evt);
      }
    });
    panel_operaciones.add(btn_division);

    btn_suma.setBackground(new java.awt.Color(204, 204, 204));
    btn_suma.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_suma.setForeground(new java.awt.Color(42, 42, 42));
    btn_suma.setText("+");
    btn_suma.setFocusable(false);
    btn_suma.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_suma.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_sumaActionPerformed(evt);
      }
    });
    panel_operaciones.add(btn_suma);

    btn_resta.setBackground(new java.awt.Color(204, 204, 204));
    btn_resta.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_resta.setForeground(new java.awt.Color(42, 42, 42));
    btn_resta.setText("-");
    btn_resta.setFocusable(false);
    btn_resta.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_resta.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_restaActionPerformed(evt);
      }
    });
    panel_operaciones.add(btn_resta);

    panel_botones.add(panel_operaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 90, 100, 310));

    panel_borrado.setBackground(new java.awt.Color(42, 42, 42));
    panel_borrado.setLayout(new java.awt.GridLayout(1, 2, 5, 5));

    btn_del.setBackground(new java.awt.Color(188, 87, 4));
    btn_del.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_del.setForeground(new java.awt.Color(42, 42, 42));
    btn_del.setText("DEL");
    btn_del.setFocusable(false);
    btn_del.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_del.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_delActionPerformed(evt);
      }
    });
    panel_borrado.add(btn_del);

    btn_ac.setBackground(new java.awt.Color(188, 87, 4));
    btn_ac.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    btn_ac.setForeground(new java.awt.Color(42, 42, 42));
    btn_ac.setText("AC");
    btn_ac.setFocusable(false);
    btn_ac.setPreferredSize(new java.awt.Dimension(50, 50));
    btn_ac.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_acActionPerformed(evt);
      }
    });
    panel_borrado.add(btn_ac);

    panel_botones.add(panel_borrado, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, 210, 70));

    getContentPane().add(panel_botones, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 130, 450, 410));

    panel_display.setBackground(new java.awt.Color(42, 42, 42));
    panel_display.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    text_resul.setEditable(false);
    text_resul.setFont(new java.awt.Font("Tahoma", 0, 32)); // NOI18N
    text_resul.setForeground(new java.awt.Color(42, 42, 42));
    text_resul.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
    text_resul.setToolTipText("Resultado...");
    text_resul.setBorder(javax.swing.BorderFactory.createEmptyBorder(8, 16, 8, 16));
    text_resul.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    text_resul.setFocusable(false);
    text_resul.setMargin(null);
    text_resul.setScrollOffset(1);
    panel_display.add(text_resul, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 430, 50));

    scrollPanel.setBorder(null);
    scrollPanel.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
    scrollPanel.setAutoscrolls(true);
    scrollPanel.setFocusable(false);
    scrollPanel.setViewportView(null);

    text_calculo.setEditable(false);
    text_calculo.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
    text_calculo.setForeground(new java.awt.Color(42, 42, 42));
    text_calculo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
    text_calculo.setAutoscrolls(false);
    text_calculo.setBorder(javax.swing.BorderFactory.createEmptyBorder(8, 16, 8, 16));
    text_calculo.setCaretColor(new java.awt.Color(51, 51, 51));
    text_calculo.setFocusable(false);
    text_calculo.setMargin(null);
    text_calculo.setScrollOffset(1);
    scrollPanel.setViewportView(text_calculo);

    panel_display.add(scrollPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 430, 70));

    getContentPane().add(panel_display, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 450, 130));

    pack();
    setLocationRelativeTo(null);
  }// </editor-fold>//GEN-END:initComponents

    private void btn_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_2ActionPerformed
      calculadora.ingresarDigito('2');
    }//GEN-LAST:event_btn_2ActionPerformed

    private void btn_9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_9ActionPerformed
      calculadora.ingresarDigito('9');
    }//GEN-LAST:event_btn_9ActionPerformed

    private void btn_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_1ActionPerformed
      calculadora.ingresarDigito('1');
    }//GEN-LAST:event_btn_1ActionPerformed

    private void btn_3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_3ActionPerformed
      calculadora.ingresarDigito('3');
    }//GEN-LAST:event_btn_3ActionPerformed

    private void btn_0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_0ActionPerformed
      calculadora.ingresarDigito('0');
    }//GEN-LAST:event_btn_0ActionPerformed

    private void btn_4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_4ActionPerformed
      calculadora.ingresarDigito('4');
    }//GEN-LAST:event_btn_4ActionPerformed

    private void btn_5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_5ActionPerformed
      calculadora.ingresarDigito('5');
    }//GEN-LAST:event_btn_5ActionPerformed

    private void btn_6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_6ActionPerformed
      calculadora.ingresarDigito('6');
    }//GEN-LAST:event_btn_6ActionPerformed

    private void btn_7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_7ActionPerformed
      calculadora.ingresarDigito('7');
    }//GEN-LAST:event_btn_7ActionPerformed

    private void btn_8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_8ActionPerformed
      calculadora.ingresarDigito('8');
    }//GEN-LAST:event_btn_8ActionPerformed

    private void btn_igualdadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_igualdadActionPerformed
      double resultado = 0;

      try {
        resultado = calculadora.calcular();
      } catch (RuntimeException e) {
        String exceptionMsj = e.getMessage();
        errorMsj(exceptionMsj);
      } finally {
        text_resul.setText(String.valueOf(resultado));
      }
    }//GEN-LAST:event_btn_igualdadActionPerformed

    private void btn_puntoDecimalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_puntoDecimalActionPerformed
      try {
        calculadora.ingresarPuntoDecimal();

      } catch (RuntimeException e) {
        String exceptionMsj = e.getMessage();
        warningMsj(exceptionMsj);
      }
    }//GEN-LAST:event_btn_puntoDecimalActionPerformed

    private void btn_delActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_delActionPerformed
      calculadora.removerUltimoElementoIngresado();
    }//GEN-LAST:event_btn_delActionPerformed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
      // numero a la Memoria
    }//GEN-LAST:event_btn_saveActionPerformed

    private void btn_memoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_memoryActionPerformed
      // Al presionar se va recuperando lo que se guardo en el array Memoria
    }//GEN-LAST:event_btn_memoryActionPerformed

    private void btn_productoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_productoActionPerformed
      try {
        calculadora.ingresarOperador('*');

      } catch (RuntimeException e) {
        String exceptionMsj = e.getMessage();
        warningMsj(exceptionMsj);
      }
    }//GEN-LAST:event_btn_productoActionPerformed

    private void btn_restaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_restaActionPerformed
      try {
        calculadora.ingresarOperador('-');

      } catch (RuntimeException e) {
        String exceptionMsj = e.getMessage();
        warningMsj(exceptionMsj);
      }
    }//GEN-LAST:event_btn_restaActionPerformed

    private void btn_sumaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sumaActionPerformed
      try {
        calculadora.ingresarOperador('+');

      } catch (RuntimeException e) {
        String exceptionMsj = e.getMessage();
        warningMsj(exceptionMsj);
      }
    }//GEN-LAST:event_btn_sumaActionPerformed

    private void btn_divisionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_divisionActionPerformed
      try {
        calculadora.ingresarOperador('/');

      } catch (RuntimeException e) {
        String exceptionMsj = e.getMessage();
        warningMsj(exceptionMsj);
      }
    }//GEN-LAST:event_btn_divisionActionPerformed

    private void btn_acActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_acActionPerformed
      calculadora.removerTodo();
    }//GEN-LAST:event_btn_acActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException | InstantiationException
            | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(
              JF_Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex
      );
    }
    //</editor-fold>

    //</editor-fold>

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new JF_Calculadora().setVisible(true);
      }
    });
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btn_0;
  private javax.swing.JButton btn_1;
  private javax.swing.JButton btn_2;
  private javax.swing.JButton btn_3;
  private javax.swing.JButton btn_4;
  private javax.swing.JButton btn_5;
  private javax.swing.JButton btn_6;
  private javax.swing.JButton btn_7;
  private javax.swing.JButton btn_8;
  private javax.swing.JButton btn_9;
  private javax.swing.JButton btn_ac;
  private javax.swing.JButton btn_del;
  private javax.swing.JButton btn_division;
  private javax.swing.JButton btn_igualdad;
  private javax.swing.JButton btn_memory;
  private javax.swing.JButton btn_producto;
  private javax.swing.JButton btn_puntoDecimal;
  private javax.swing.JButton btn_resta;
  private javax.swing.JButton btn_save;
  private javax.swing.JButton btn_suma;
  private javax.swing.JPanel panel_borrado;
  private javax.swing.JPanel panel_botones;
  private javax.swing.JPanel panel_display;
  private javax.swing.JPanel panel_memoria;
  private javax.swing.JPanel panel_numeros;
  private javax.swing.JPanel panel_operaciones;
  private javax.swing.JScrollPane scrollPanel;
  private javax.swing.JTextField text_calculo;
  private javax.swing.JTextField text_resul;
  // End of variables declaration//GEN-END:variables

  @Override
  public void update(Cuenta _cuenta) {
    text_calculo.setText(_cuenta.toString());
  }

  private void errorMsj(String _msj) {
    JOptionPane.showMessageDialog(
            this,
            _msj,
            "Error al calcular",
            JOptionPane.ERROR_MESSAGE
    );
  }

  private void warningMsj(String _msj) {
    JOptionPane.showMessageDialog(
            this,
            _msj,
            "Operación invalida",
            JOptionPane.WARNING_MESSAGE
    );
  }
}
