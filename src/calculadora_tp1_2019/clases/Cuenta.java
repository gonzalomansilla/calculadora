package calculadora_tp1_2019.clases;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Cuenta {

  private ArrayList<StringBuffer> cuenta;

  public Cuenta() {
    cuenta = new ArrayList<>();
  }

  public Cuenta(List<StringBuffer> _cuenta) {
    cuenta = new ArrayList<>(_cuenta);
  }

  public void agregarDigito(char _digito) {
    if (cuenta.isEmpty()) {
      crearElementoCon(_digito);
    } else if (esUnOperador(consultarUltimoElemento())) {
      boolean esElPrimerElemento = cuenta.size() == 1;

      if (esElPrimerElemento || hayDobleOperadorValido()) {
        getUltimoElemento().append(_digito);
      } else {
        crearElementoCon(_digito);
      }

    } else {
      getUltimoElemento().append(_digito);
    }
  }

  public void agregarOperador(char _operador) {
    if (noEsUnSegundoOperadorValido(_operador)) {
      throw new RuntimeException("Segundo operador invalido");

    } else if (hayDobleOperadorValido()) {
      throw new RuntimeException("No es posible ingresar más de dos operadores consecutivos");

    } else if (cuenta.isEmpty()) {
      if (_operador == '+' || _operador == '-') {
        crearElementoCon(_operador);
      } else {
        throw new RuntimeException("Ingrese números para poder calcular");
      }

    } else if (consultarUltimoElemento().length() > 0) {
      crearElementoCon(_operador);
    }
  }

  public void eliminarUltimoElementoIngresado() {
    if (cuenta.isEmpty()) {
      return;
    }

    int largoElemento = consultarUltimoElemento().length();

    if (largoElemento <= 1) {
      cuenta.remove(cuenta.size() - 1);
    } else {
      getUltimoElemento().deleteCharAt(largoElemento - 1);
    }
  }

  public void eliminarCuenta() {
    cuenta.clear();
  }

  public void agregarPuntoDecimal() {
    char punto = '.';

    if (hayDobleOperadorValido()) {
      getUltimoElemento().append('0').append(punto);

    } else if (cantidadDeElementos() == 0 || esUnOperador(consultarUltimoElemento())) {
      cuenta.add(new StringBuffer().append('0').append(punto));

    } else if (!seIngresoUnPunto()) {
      getUltimoElemento().append(punto);

    } else {
      throw new RuntimeException("Doble punto decimal invalido");
    }
  }

  public void agregarElementoEnIndice(int _indice, StringBuffer _elemento) {
    cuenta.add(_indice, _elemento);
  }

  private StringBuffer getUltimoElemento() {
    if (cuenta.isEmpty()) {
      return new StringBuffer("");
    }

    return cuenta.get(cuenta.size() - 1);
  }

  public List<StringBuffer> obtenerCuentaDesdeIndice(int _indiceDeInicio) {
    return cuenta.subList(_indiceDeInicio, cuenta.size());
  }

  public int cantidadDeElementos() {
    return cuenta.size();
  }

  public String consultarUltimoElemento() {
    return (cuenta.isEmpty()) ? " " : getUltimoElemento().toString();
  }

  public String consultarElementoEn(int _index) {
    return _index < cuenta.size() ? cuenta.get(_index).toString() : "";
  }

  public ArrayList<StringBuffer> getCuenta() {
    return (ArrayList<StringBuffer>) cuenta.clone();
  }

  @Override
  public String toString() {
    if (cuenta.isEmpty()) {
      return "";
    }

    StringBuffer resul = new StringBuffer();

    cuenta.forEach((elem) -> {
      resul.append(elem.toString()).append(" ");
    });

    resul.deleteCharAt(resul.length() - 1);
    return resul.toString();
  }

  private void crearElementoCon(char _caracter) {
    cuenta.add(new StringBuffer("").append(_caracter));
  }

  private boolean esUnOperador(String _elemento) {
    return Pattern.matches("[+-/*]?", _elemento);
  }

  private boolean hayDobleOperadorValido() {
    if (cuenta.size() >= 2) {
      boolean hayUnPrimerOperador = esUnOperador(cuenta.get(cuenta.size() - 2).toString());
      boolean ultimoOperadorEsValido = consultarUltimoElemento().matches("[+-]?");

      return (hayUnPrimerOperador) ? ultimoOperadorEsValido : false;
    }
    return false;
  }

  private boolean noEsUnSegundoOperadorValido(char _operador) {
    if (esUnOperador(consultarUltimoElemento())) {
      return (_operador == '*' || _operador == '/');
    }
    return false;
  }

  private boolean seIngresoUnPunto() {
    StringBuffer ultimoNumero = getUltimoElemento();

    for (int c = 0; c < ultimoNumero.length(); c++) {
      char caracter = ultimoNumero.charAt(c);

      if (caracter == '.') {
        return true;
      }
    }
    return false;
  }
  
}
