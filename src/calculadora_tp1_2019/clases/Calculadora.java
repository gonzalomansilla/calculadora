package calculadora_tp1_2019.clases;

import calculadora_tp1_2019.interfaces.Observable;
import calculadora_tp1_2019.interfaces.Observer;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class Calculadora implements Observable {

  private Cuenta cuenta;
  private Observer observer;
  private final DecimalFormat formatoDeNumero;
  private double resultado;

  public Calculadora() {
    cuenta = new Cuenta();

    DecimalFormatSymbols separadorPunto = new DecimalFormatSymbols();
    separadorPunto.setDecimalSeparator('.');
    formatoDeNumero = new DecimalFormat("#.####", separadorPunto);
  }

  public double calcular() {
    if (cuenta.getCuenta().isEmpty()) {
      return resultado;
    } else if (seIngresoSoloUnNumero()) {
      return (resultado = formatearNumero(resultado));
    } else if (hayOperadorSinAlgunoDeSusOperandos()) {
      throw new RuntimeException("Un operador no cuenta con uno o ninguno de sus operandos");
    } else {
      double operando1 = Double.parseDouble(cuenta.consultarElementoEn(0));
      String operador = cuenta.consultarElementoEn(1);
      double operando2 = Double.parseDouble(cuenta.consultarElementoEn(2));
      String siguienteOperador = cuenta.consultarElementoEn(3);

      switch (operador) {
        case "/":
          double divisionActual;
          if (esCero(operando2)) {
            throw new ArithmeticException("No es posible resolver. Se esta dividiendo por cero");
          } else {
            divisionActual = dividir(operando1, operando2);

            if (!siguienteOperador.equals("")) {
              double resultAux;
              Cuenta cuentaRestante = new Cuenta(cuenta.obtenerCuentaDesdeIndice(3));
              cuentaRestante.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(divisionActual)));

              do {
                cuentaRestante = calcular(cuentaRestante);
              } while (cuentaRestante.cantidadDeElementos() > 1);

              resultAux = Double.parseDouble(cuentaRestante.consultarUltimoElemento());
              resultado = formatearNumero(resultAux);
              return resultado;
            }
            resultado = formatearNumero(divisionActual);
            return resultado;
          }
        case "*":
          double multiplicacionActual = multiplicar(operando1, operando2);

          if (!siguienteOperador.equals("")) {
            double resultAux;
            Cuenta cuentaRestante = new Cuenta(cuenta.obtenerCuentaDesdeIndice(3));
            cuentaRestante.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(multiplicacionActual)));

            do {
              cuentaRestante = calcular(cuentaRestante);
            } while (cuentaRestante.cantidadDeElementos() > 1);

            resultAux = Double.parseDouble(cuentaRestante.consultarUltimoElemento());
            resultado = formatearNumero(resultAux);
            return resultado;
          }
          resultado = formatearNumero(multiplicacionActual);
          ;
          return resultado;
        case "+":
          double resultAux;

          if (!siguienteOperador.equals("")) {
            double sumaActual;
            Cuenta cuentaRestante;

            if (siguienteOperador.equals("+") || siguienteOperador.equals("-")) {
              sumaActual = sumar(operando1, operando2);
              cuentaRestante = new Cuenta(cuenta.obtenerCuentaDesdeIndice(3));
              cuentaRestante.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(sumaActual)));

              do {
                cuentaRestante = calcular(cuentaRestante);
              } while (cuentaRestante.cantidadDeElementos() > 1);

              resultAux = Double.parseDouble(cuentaRestante.consultarUltimoElemento());
              resultado = formatearNumero(resultAux);
              return resultado;

            } else if (siguienteOperador.equals("*") || siguienteOperador.equals("/")) {
              cuentaRestante = new Cuenta(cuenta.obtenerCuentaDesdeIndice(2));

              cuentaRestante = calcular(cuentaRestante);
              cuentaRestante.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(operando1)));
              cuentaRestante.agregarElementoEnIndice(1, new StringBuffer(String.valueOf(operador)));

              do {
                cuentaRestante = calcular(cuentaRestante);

              } while (cuentaRestante.cantidadDeElementos() > 1);

              resultAux = Double.parseDouble(cuentaRestante.consultarUltimoElemento());
              resultado = formatearNumero(resultAux);
              return resultado;
            }
          }

          resultAux = sumar(operando1, operando2);
          resultado = formatearNumero(resultAux);
          return resultado;
        case "-":
          if (!siguienteOperador.equals("")) {
            double restaActual;
            Cuenta cuentaRestante;

            if (siguienteOperador.equals("+") || siguienteOperador.equals("-")) {
              restaActual = restar(operando1, operando2);
              cuentaRestante = new Cuenta(cuenta.obtenerCuentaDesdeIndice(3));
              cuentaRestante.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(restaActual)));

              do {
                cuentaRestante = calcular(cuentaRestante);
              } while (cuentaRestante.cantidadDeElementos() > 1);

              resultAux = Double.parseDouble(cuentaRestante.consultarUltimoElemento());
              resultado = formatearNumero(resultAux);
              return resultado;

            } else if (siguienteOperador.equals("*") || siguienteOperador.equals("/")) {
              cuentaRestante = new Cuenta(cuenta.obtenerCuentaDesdeIndice(2));

              cuentaRestante = calcular(cuentaRestante);
              cuentaRestante.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(operando1)));
              cuentaRestante.agregarElementoEnIndice(1, new StringBuffer(String.valueOf(operador)));

              do {
                cuentaRestante = calcular(cuentaRestante);
              } while (cuentaRestante.cantidadDeElementos() > 1);

              resultAux = Double.parseDouble(cuentaRestante.consultarUltimoElemento());
              resultado = formatearNumero(resultAux);
              return resultado;
            }
          }

          resultAux = restar(operando1, operando2);
          resultado = formatearNumero(resultAux);
          return resultado;
        default:
          break;
      }
    }
    return resultado;
  }

  private Cuenta calcular(Cuenta _cuenta) {
    if (_cuenta.cantidadDeElementos() == 1) {
      return _cuenta;
    }

    double operando1 = Double.parseDouble(_cuenta.consultarElementoEn(0));
    String operador = _cuenta.consultarElementoEn(1);
    double operando2 = Double.parseDouble(_cuenta.consultarElementoEn(2));
    String siguienteOperador = _cuenta.consultarElementoEn(3);

    switch (operador) {
      case "/":
        double divisionActual;
        if (esCero(operando2)) {
          throw new ArithmeticException("No es posible resolver. Se esta dividiendo por cero");
        } else {
          divisionActual = dividir(operando1, operando2);

          if (!siguienteOperador.equals("")) {
            Cuenta cuentaRestante = new Cuenta(_cuenta.obtenerCuentaDesdeIndice(3));
            cuentaRestante.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(divisionActual)));
            return cuentaRestante;
          }

          Cuenta cuentaConResultado = new Cuenta();
          cuentaConResultado.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(divisionActual)));
          return cuentaConResultado;
        }
      case "*":
        double multiplicacionActual = multiplicar(operando1, operando2);

        if (!siguienteOperador.equals("")) {
          Cuenta cuentaRestante = new Cuenta(_cuenta.obtenerCuentaDesdeIndice(3));
          cuentaRestante.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(multiplicacionActual)));
          return cuentaRestante;
        }

        Cuenta cuentaConResultado = new Cuenta();
        cuentaConResultado.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(multiplicacionActual)));
        return cuentaConResultado;
      case "+":
        double sumaActual = sumar(operando1, operando2);

        if (!siguienteOperador.equals("")) {
          Cuenta cuentaRestante;

          if (siguienteOperador.equals("+") || siguienteOperador.equals("-")) {
            cuentaRestante = new Cuenta(_cuenta.obtenerCuentaDesdeIndice(3));
            cuentaRestante.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(sumaActual)));
            cuentaRestante = calcular(cuentaRestante);
            return cuentaRestante;

          } else if (siguienteOperador.equals("*") || siguienteOperador.equals("/")) {
            cuentaRestante = new Cuenta(_cuenta.obtenerCuentaDesdeIndice(2));
            cuentaRestante = calcular(cuentaRestante);
            cuentaRestante.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(operando1)));
            cuentaRestante.agregarElementoEnIndice(1, new StringBuffer(String.valueOf(operador)));
            return cuentaRestante;
          }
        }

        cuentaConResultado = new Cuenta();
        cuentaConResultado.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(sumaActual)));
        return cuentaConResultado;
      case "-":
        double restaActual = restar(operando1, operando2);

        if (!siguienteOperador.equals("")) {
          Cuenta cuentaRestante;

          if (siguienteOperador.equals("+") || siguienteOperador.equals("-")) {
            cuentaRestante = new Cuenta(_cuenta.obtenerCuentaDesdeIndice(3));
            cuentaRestante.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(restaActual)));
            cuentaRestante = calcular(cuentaRestante);
            return cuentaRestante;

          } else if (siguienteOperador.equals("*") || siguienteOperador.equals("/")) {
            cuentaRestante = new Cuenta(_cuenta.obtenerCuentaDesdeIndice(2));
            cuentaRestante = calcular(cuentaRestante);
            cuentaRestante.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(operando1)));
            cuentaRestante.agregarElementoEnIndice(1, new StringBuffer(String.valueOf(operador)));
            return cuentaRestante;
          }
        }
        cuentaConResultado = new Cuenta();
        cuentaConResultado.agregarElementoEnIndice(0, new StringBuffer(String.valueOf(restaActual)));
        return cuentaConResultado;
    }
    return new Cuenta();
  }

  public void ingresarDigito(char _digito) {
    cuenta.agregarDigito(_digito);
    notificarObservadores();
  }

  public void ingresarOperador(char _operador) {
    cuenta.agregarOperador(_operador);
    notificarObservadores();
  }

  public void ingresarPuntoDecimal() {
    cuenta.agregarPuntoDecimal();
    notificarObservadores();
  }

  public void removerUltimoElementoIngresado() {
    cuenta.eliminarUltimoElementoIngresado();
    notificarObservadores();
  }

  public void removerTodo() {
    cuenta.eliminarCuenta();
    resultado = 0.0;
    notificarObservadores();
  }

  private double sumar(double _num1, double _num2) {
    return _num1 + _num2;
  }

  private double restar(double _num1, double _num2) {
    return _num1 - _num2;
  }

  private double multiplicar(double _num1, double _num2) {
    return _num1 * _num2;
  }

  private double dividir(double _num1, double _num2) {
    return _num1 / _num2;
  }

  public String consultarCuenta() {
    return cuenta.toString();
  }

  public String consultarUltimoElemento() {
    return cuenta.consultarUltimoElemento();
  }

  public String consultarResultado() {
    return String.valueOf(resultado);
  }

  public ArrayList<StringBuffer> getCuenta() {
    return cuenta.getCuenta();
  }

  @Override
  public void establecerObserver(Observer _observer) {
    observer = _observer;
  }

  private void notificarObservadores() {
    if (observer == null) {
      return;
    }

    observer.update(cuenta);
  }

  private double formatearNumero(double _numero) {
    return Double.parseDouble(formatoDeNumero.format(_numero));
  }

  private boolean hayOperadorSinAlgunoDeSusOperandos() {
    boolean ultimoElementoEsUnOperador = cuenta.consultarUltimoElemento().matches("[+-/*]?");

    if (cuenta.cantidadDeElementos() == 1) {
      if (ultimoElementoEsUnOperador) {
        return true;
      }
    } else if (ultimoElementoEsUnOperador) {
      return true;
    }
    return false;
  }

  private boolean seIngresoSoloUnNumero() {
    if (cuenta.cantidadDeElementos() == 1) {
      resultado = Double.parseDouble(cuenta.consultarUltimoElemento());
      return true;
    }
    return false;
  }

  private boolean esCero(double _operando) {
    return _operando == 0;
  }

}
