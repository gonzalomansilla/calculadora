package calculadora_tp1_2019.interfaces;

public interface Observable {

  public void establecerObserver(Observer _observer);
}
