package calculadora_tp1_2019.interfaces;

import calculadora_tp1_2019.clases.Cuenta;

public interface Observer {

  public void update(Cuenta _cuenta);

}
