package calculadora_tp1_2019.clases.calculadora;

import calculadora_tp1_2019.clases.Calculadora;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class CuentasDeUnOperadorTest {

  private Calculadora calculadora;

  @Before
  public void setUp() {
    calculadora = new Calculadora();
  }

  @Test
  public void mostrarUnicoNumeroIngresado() throws Exception {
    calculadora.ingresarDigito('5');
    calculadora.ingresarDigito('0');

    assertEquals(50, (int) calculadora.calcular());
  }

  @Test
  public void calcularDivision() throws Exception {
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('2');
    calculadora.ingresarOperador('/');
    calculadora.ingresarDigito('6');

    assertEquals(2, (int) calculadora.calcular());
  }

  @Test
  public void calcularMultplicacion() throws Exception {
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('2');
    calculadora.ingresarOperador('*');
    calculadora.ingresarDigito('2');

    assertEquals(24, (int) calculadora.calcular());
  }

  @Test
  public void calcularSuma() throws Exception {
    calculadora.ingresarDigito('1');
    calculadora.ingresarOperador('+');
    calculadora.ingresarDigito('2');

    assertEquals(3, (int) calculadora.calcular());
  }

  @Test
  public void calcularResta() throws Exception {
    calculadora.ingresarDigito('1');
    calculadora.ingresarOperador('-');
    calculadora.ingresarDigito('2');

    assertEquals(-1, (int) calculadora.calcular());
  }
}
