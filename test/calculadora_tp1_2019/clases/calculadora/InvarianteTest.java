package calculadora_tp1_2019.clases.calculadora;

import calculadora_tp1_2019.clases.Calculadora;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class InvarianteTest {

  private Calculadora calculadora;

  @Before
  public void setUp() {
    calculadora = new Calculadora();
  }

  @Test(expected = RuntimeException.class)
  public void impedirOperadorComoUltimoElemento() {
    calculadora.ingresarDigito('1');
    calculadora.ingresarOperador('+');
    calculadora.ingresarDigito('2');
    calculadora.ingresarOperador('-');

    assertEquals(0, calculadora.calcular());
  }

  @Test(expected = RuntimeException.class)
  public void impedirOperadorComoUnicoElemento() {
    calculadora.ingresarOperador('+');

    assertEquals(0, calculadora.calcular());
  }

  @Test(expected = RuntimeException.class)
  public void impedirMultiplicarComoPrimerEntrada() {
    calculadora.ingresarOperador('*');
  }

  @Test(expected = RuntimeException.class)
  public void impedirTresOperadoresConcecutivos() {
    calculadora.ingresarDigito('1');
    calculadora.ingresarOperador('*');
    calculadora.ingresarOperador('-');
    calculadora.ingresarOperador('+');
  }

  @Test(expected = RuntimeException.class)
  public void impedirSegundoOperadorInvalido() {
    calculadora.ingresarDigito('1');
    calculadora.ingresarOperador('-');
    calculadora.ingresarOperador('*');

    Calculadora calculadora2 = new Calculadora();
    calculadora2.ingresarDigito('2');
    calculadora2.ingresarOperador('+');
    calculadora2.ingresarOperador('/');
  }

  @Test(expected = RuntimeException.class)
  public void impedirDivisionComoPrimerEntrada() {
    calculadora.ingresarOperador('/');
  }

  @Test(expected = ArithmeticException.class)
  public void impedirDivisionPorCero() {
    calculadora.ingresarDigito('1');
    calculadora.ingresarOperador('/');
    calculadora.ingresarDigito('0');

    assertEquals(0, calculadora.calcular());
  }

  @Test(expected = RuntimeException.class)
  public void impedirDoblePuntoDecimalConsecutivo() {
    calculadora.ingresarPuntoDecimal();
    calculadora.ingresarPuntoDecimal();
    calculadora.ingresarDigito('1');
  }

  @Test(expected = RuntimeException.class)
  public void impedirDoblePuntoDecimalEnNumero() {
    calculadora.ingresarDigito('3');
    calculadora.ingresarPuntoDecimal();
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('4');
    calculadora.ingresarPuntoDecimal();
  }

}
