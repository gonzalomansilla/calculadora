package calculadora_tp1_2019.clases.calculadora;

import calculadora_tp1_2019.clases.Calculadora;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class CalculadoraTest {

  private Calculadora calculadora;

  @Before
  public void setUp() {
    calculadora = new Calculadora();
  }

  @Test
  public void cuentaConJerarquiaDeOperadores() {
    calculadora.ingresarDigito('5');
    calculadora.ingresarOperador('+');
    calculadora.ingresarDigito('6');
    calculadora.ingresarOperador('/');
    calculadora.ingresarDigito('5');
    calculadora.ingresarOperador('*');
    calculadora.ingresarDigito('4');
    calculadora.ingresarOperador('-');
    calculadora.ingresarDigito('3');
    calculadora.ingresarOperador('+');
    calculadora.ingresarDigito('2');
    calculadora.ingresarOperador('-');
    calculadora.ingresarDigito('3');
    calculadora.ingresarOperador('/');
    calculadora.ingresarDigito('5');
    calculadora.ingresarOperador('+');
    calculadora.ingresarDigito('9');
    
    assertEquals(17.2, calculadora.calcular(), 0.0);
  }

  @Test
  public void cuentaConSumaResta() {
    calculadora.ingresarDigito('2');
    calculadora.ingresarOperador('+');
    calculadora.ingresarDigito('2');
    calculadora.ingresarOperador('-');
    calculadora.ingresarDigito('6');

    assertEquals(-2, (int) calculadora.calcular());
  }

  @Test
  public void cuentaConNumerosDecimales(){
    calculadora.ingresarDigito('3');
    calculadora.ingresarPuntoDecimal();
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('4');
    calculadora.ingresarOperador('+');
    calculadora.ingresarDigito('3');
    calculadora.ingresarPuntoDecimal();
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('4');
    
    assertEquals(6.28, calculadora.calcular(), 0.0);
  }
  
  @Test
  public void puntoDecimalComoPrimeraEntradaDeNumero(){
    calculadora.ingresarPuntoDecimal();
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('4');
    
    assertEquals(0.14, calculadora.calcular(), 0.0);
  }
  
  @Test
  public void ingresarDigitoConSegundoOperadorValido() {
    calculadora.ingresarDigito('1');
    calculadora.ingresarOperador('*');
    calculadora.ingresarOperador('-');
    calculadora.ingresarDigito('2');

    assertEquals(3, calculadora.getCuenta().size());
    
    Calculadora calculadora2 = new Calculadora();
    calculadora2.ingresarDigito('1');
    calculadora2.ingresarOperador('/');
    calculadora2.ingresarOperador('+');
    calculadora2.ingresarDigito('2');

    assertEquals(3, calculadora2.getCuenta().size());
  }

  //Happy Paths
  @Test
  public void numeroDecimalConDobleOperador(){
    calculadora.ingresarDigito('1');
    calculadora.ingresarOperador('+');
    calculadora.ingresarOperador('-');
    calculadora.ingresarPuntoDecimal();
    calculadora.ingresarDigito('5');
    
    assertEquals(0.5, calculadora.calcular(), 0.0);
  }
  
  @Test
  public void ingresarPrimerNumeroDecimal(){
    calculadora.ingresarDigito('3');
    calculadora.ingresarPuntoDecimal();
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('4');
    
    assertEquals("3.14", calculadora.consultarUltimoElemento());
  }
  
  @Test
  public void tresElementosEnUnaOperacionBasica() {
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('2');
    calculadora.ingresarOperador('+');
    calculadora.ingresarDigito('3');
    calculadora.ingresarDigito('4');

    assertEquals(3, calculadora.getCuenta().size());
  }

  @Test
  public void mostrarCuenta() {
    calculadora.ingresarDigito('1');
    calculadora.ingresarOperador('+');
    calculadora.ingresarDigito('3');
    calculadora.ingresarOperador('*');
    calculadora.ingresarDigito('4');

    assertEquals("1 + 3 * 4", calculadora.consultarCuenta());
  }

}
