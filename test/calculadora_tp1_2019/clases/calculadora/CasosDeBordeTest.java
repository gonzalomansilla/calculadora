package calculadora_tp1_2019.clases.calculadora;

import calculadora_tp1_2019.clases.Calculadora;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class CasosDeBordeTest {

  private Calculadora calculadora;

  @Before
  public void setUp() {
    calculadora = new Calculadora();
  }

  @Test
  public void sinElementosIngresados() {
    assertEquals(0, (int) calculadora.calcular());
  }

  @Test
  public void removerUltimoDigitoIngresado() {
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('2');
    calculadora.ingresarDigito('3');

    calculadora.removerUltimoElementoIngresado();

    assertEquals("12", calculadora.consultarUltimoElemento());
  }

  @Test
  public void removerUltimoOperadorIngresado() {
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('2');
    calculadora.ingresarOperador('+');

    calculadora.removerUltimoElementoIngresado();

    assertEquals("12", calculadora.consultarUltimoElemento());
  }

  @Test
  public void removerUltimoDigitoDeUnElemento() {
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('2');
    calculadora.ingresarOperador('+');
    calculadora.ingresarDigito('1');

    calculadora.removerUltimoElementoIngresado();

    assertEquals("+", calculadora.consultarUltimoElemento());
  }

  @Test
  public void numeroPositivoComoPrimerEntrada() {
    calculadora.ingresarOperador('+');
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('2');

    assertEquals("+12", calculadora.consultarUltimoElemento());
  }

  @Test
  public void numeroNegativoComoPrimerEntrada() {
    calculadora.ingresarOperador('-');
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('2');

    assertEquals("-12", calculadora.consultarUltimoElemento());
  }

  @Test
  public void ingresarUnPrimerDigito() {
    calculadora.ingresarDigito('1');

    assertEquals("1", calculadora.consultarUltimoElemento());
  }

  @Test
  public void consultarUltimoElementoIngresado() {
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('2');

    assertEquals("12", calculadora.consultarUltimoElemento());
  }

  @Test
  public void unElementoAlIngresarDigitos() {
    calculadora.ingresarDigito('1');
    calculadora.ingresarDigito('2');
    calculadora.ingresarDigito('3');
    calculadora.ingresarDigito('4');

    assertEquals(1, calculadora.getCuenta().size());
  }

}
