﻿# Calculadora

_Trabajo Practico n°1 para la materia Programación III_

__Integrantes:__ Gonzalo Mansilla

## IREP

* Solo se puede ingresar dos operadores de forma consecutivas, mientras estos sean validos entre si.
* No será posible ingresar último un operador al momento de calcular.
* No será posible dejar solo un operador al momento de calcular.
* No será posible tener una multiplicación (*) o división (/) como primera entrada de una cuenta.
* No será posible ingresar una multiplicación (*) o división (/) luego de una suma (+) o resta (-).
* No será posible ingresar doble punto decimal en un número.
* No será posible ingresar dos puntos decimales de forma consecutiva.
* Se formateara a solo cuatro dígitos decimales el resultado obtenido.

## Implementación

* Se realizó con 2 clases, `Calculadora` y `Cuenta` respectivamente
* Para almacenar los números y operandos se usó un ArraysList de `BufferString`
* Se realizaron distintos Tests, donde para cada objetivo se tendría un Set distinto.
* Dado que se trabaja con decimales se requería un control sobre la cantidad de decimales con las que se deseaba trabajar, por lo que se utilizó `DecimalFormat`, de la librería de java, que dado un double es posible obtener otro con el formateo deseado. Cabe destacar que el formateo solo se aplicó al momento de obtener un resultado, por lo que en la resolución de una cuenta se utilizó la capacidad máxima de un double.
Para este caso se decidió utilizar solo __cuatro__ decimales como máximo al momento de obtener un resultado.
* Para una mejor representación de los números decimales se utilizó double en vez de float. `BigDecimal` se lo descarto debido a que no se requería una gran precisión, y además, porque aumentaba la complejidad del código para obtener y establecer los distintos resultados.

## Decisiciones tomadas

* Para el almacenamiento de los dígitos y operandos, primero se lo pensó en ir insertándolos individualmente en una posición nueva a medida que se los introducía, pero se vio que esta implementación requería mucho trabajo al momento de obtener los números correspondientes y resolver el calculo.
Finalmente se optó por almacenarlos en un ArrayList de `StringBuffer`, donde cada digito se iría insertando en su elemento correspondiente hasta formar el número deseado. En cambio los operadores ocuparían una posición cada uno.
* Cuando se tuvo cuentas del estilo __2 - 2 * 2 + 4__ el calculo daba un resultado incorrecto, ya que el resultado de la división/multiplicación se sumaba/restaba con el último número, y no con el primero, es decir, que no se respetaba el orden en la resolución de la cuenta.
Para resolver esto se modificó el método `calculo()`, donde en cada multiplicación o división se retornaba el resultado con la cuenta restante. Cabe destacar que en un principio se calculaba de forma recursiva, donde cada sub-cuenta individual (Operando Operador Operando) se resolvía y luego se pasaba a resolver lo demás, para finalmente ir retornando los resultados hacia "arriba" para ser sumado o restado con las anteriores ya resueltas.
* Cuando se ingresaba muchos números en el campo de texto el mismo se desbordaba y generaba automáticamente un Scroll horizontal, pero este se mantenía al principio de la cuenta introducida, por lo que el usuario no podía ver lo que iba introduciendo. Para solucionar esto se decidió usar una clase externa llamada `SmartScroller` (ya que Java no proporcionaba esta funcionalidad) que facilitaba la lógica de dicha funcionalidad. Esta luego solo tuvo que se implementada.
* Al ir implementando tests, se notó que se mezclaban casos que verificaban excepciones con test simples de cálculo, por eso, se decidió separarlos en Clases de tests distintas.

![Imagen 1][1] ![Imagen 2][2]

  [1]: https://i.imgur.com/22c3LWR.png
  [2]: https://i.imgur.com/HQwmXCu.png
